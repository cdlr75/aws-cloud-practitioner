# AWS Cloud Practitioner

Notes for the AWS Cloud Practitioner certifications

- Summarize the working definition of AWS

- Differentiate between on-premises, hybrid-cloud, and all-in cloud

**What is cloud computing?**

→ On-demand delivery of IT resources and applications through the internet with **pay-as-you-go pricing**.

**How does the scale of cloud computing help you to save costs?**

→ The aggregated cloud usage from a large number of customers results in lower pay-as-you-go prices.

- Describe the basic global infrastructure of the AWS Cloud

- Explain the six benefits of the AWS Cloud

   - Trade upfront expense for variable expense (Upfront expense refers to data centers, physical servers, and other resources that you would need to invest in before using them.)
   - Stop spending money to run and maintain data centers
   - Stop guessing capacity
   - Benefit from massive economies of scale
   - Increase speed and agility
   - Go global in minutes

- Describe and provide an example of the core AWS services, including compute, network, databases, and storage

- Identify an appropriate solution using AWS Cloud services with various use cases

- Describe the AWS Well-Architected Framework

- Explain the shared responsibility model

- Describe the core security services within the AWS Cloud

- Describe the basics of AWS Cloud migration

- Articulate the financial benefits of the AWS Cloud for an organization’s cost management

- Define the core billing, account management, and pricing models

- Explain how to use pricing tools to make cost-effective choices for AWS services



## Amazon EC2 instance types

- General purpose instances (balance of compute, memory, and networking resources):
    - application servers
    - gaming servers
    - backend servers for enterprise applications
    - small and medium databases

- Compute optimized instances (high-performance processors):
    - high-performance web servers, compute-intensive applications servers, and dedicated gaming servers
    - optimized instances for batch processing workloads

- Memory optimized instances

- Accelerated computing instances (hardware accelerators, or coprocessors):
    - floating-point number calculations
    - graphics processing
    - data pattern matching.

- Storage optimized instances (many IOPS)
    - distributed file systems
    - data warehousing applications
    - high-frequency online transaction processing (OLTP) systems

## Amazon EC2 pricing

- **On-Demand**: ideal for short-term, irregular workloads that cannot be interrupted. No upfront costs or minimum contracts apply. The instances run continuously until you stop them, and you pay for only the compute time you use.
- **Amazon EC2 Savings Plans**: enable you to reduce your compute costs by committing to a consistent amount of compute usage for a 1-year or 3-year term. This term commitment results in savings of up to 66% over On-Demand costs.
- **Reserved Instances**: You can purchase Standard Reserved and Convertible Reserved Instances for a 1-year or 3-year term, and Scheduled Reserved Instances for a 1-year term.
- **Spot Instances**: Spot Instances are ideal for workloads with flexible start and end times, or that can withstand interruptions. Spot Instances use unused Amazon EC2 computing capacity and offer you cost savings at up to 90% off of On-Demand prices.
- **Dedicated Hosts**: You can use your existing per-socket, per-core, or per-VM software licenses to help maintain license compliance. Dedicated Hosts are the most expensive.

## Scaling Amazon EC2

## Directing traffic with Elastic Load Balancing

## Amazon Simple Notification Service (Amazon SNS)

publish/subscribe service. Using Amazon SNS topics, a publisher publishes messages to subscribers.

## Amazon Simple Queue Service (Amazon SQS)

a message queuing service.

## Serverless computing

lambda

## Amazon Elastic Container Service (Amazon ECS)

Amazon Elastic Container Service (Amazon ECS) is a highly scalable, high-performance container management system that enables you to run and scale containerized applications on AWS.

## Amazon Elastic Kubernetes Service (Amazon EKS)

a fully managed service that you can use to run Kubernetes on AWS.

## AWS Fargate

a serverless compute engine for containers. It works with both Amazon ECS and Amazon EKS.

## AWS global infrastructure

→ Selecting a Region:
    - Compliance with data governance and legal requirements
    - Proximity to your customers
    - Available services within a Region
    - Pricing

**Availability Zones**: a single data center or a group of data centers within a Region. Availability Zones are located tens of miles apart from each other. This is close enough to have low latency (the time between when content requested and received) between Availability Zones. However, if a disaster occurs in one part of the Region, they are distant enough to reduce the chance that multiple Availability Zones are affected.

A best practice is to run applications across at least two Availability Zones in a Region.

An **edge location** is a site that **Amazon CloudFront** uses to store cached copies of your content closer to your customers for faster delivery.

### Ways to interact with AWS services

- AWS Management Console (Web)
- AWS CLI
- SDK

## AWS Elastic Beanstalk

With AWS Elastic Beanstalk, you provide code and configuration settings, and Elastic Beanstalk deploys the resources necessary to perform the following tasks:
- Adjust capacity
- Load balancing
- Automatic scaling
- Application health monitoring

## AWS CloudFormation

With AWS CloudFormation, you can treat your infrastructure as code. This means that you can build an environment by writing lines of code instead of using the AWS Management Console to individually provision resources.

AWS CloudFormation provisions your resources in a safe, repeatable manner, enabling you to frequently build your infrastructure and applications without having to perform manual actions. It determines the right operations to perform when managing your stack and rolls back changes automatically if it detects errors.

# Networking

## Amazon Virtual Private Cloud (Amazon VPC)
enables to provision an isolated section of the AWS Cloud.

To allow public traffic from the internet to access your VPC, you attach an **internet gateway** to the VPC.

To access private resources in a VPC, you can use a **virtual private gateway**.
A virtual private gateway enables you to establish a virtual private network (VPN) connection between your VPC and a private network (over internet), such as an on-premises data center or internal corporate network. A virtual private gateway allows traffic into the VPC only if it is coming from an approved network.

**AWS Direct Connect** is a service that enables you to establish a dedicated private connection between your data center and a VPC.
The private connection that AWS Direct Connect provides helps you to reduce network costs and increase the amount of bandwidth that can travel through your network.

**A subnet** is a section of a VPC in which you can group resources based on security or operational needs. Subnets can be public or private.
**Public subnets** contain resources that need to be accessible by the public, such as an online store’s website.
**Private subnets** contain resources that should be accessible only through your private network, such as a database that contains customers’ personal information and order histories.

The VPC component that checks packet permissions for subnets is a **network access control list (ACL)**.
A network access control list (ACL) is a virtual firewall that controls inbound and outbound traffic at the subnet level.

Network ACLs perform **stateless packet filtering**. They remember nothing and check packets that cross the subnet border each way: inbound and outbound. By default, it allows all inbound and outbound traffic.

The VPC component that checks packet permissions for an Amazon EC2 instance is a **security group**.
A security group is a virtual firewall that controls inbound and outbound traffic for an Amazon EC2 instance.

Security groups perform **stateful packet filtering**. They remember previous decisions made for incoming packets.
By default, a security group denies all inbound traffic and allows all outbound traffic. You can add custom rules to configure which traffic to allow or deny.

## Global networking

**Amazon Route 53** is a DNS web service. It gives developers and businesses a reliable way to route end users to internet applications hosted in AWS.

# Storage & db

## Instance stores and Amazon Elastic Block Store (Amazon EBS)

An **instance store** provides temporary block-level storage for an Amazon EC2 instance. An instance store is disk storage that is physically attached to the host computer for an EC2 instance, and therefore has the same lifespan as the instance. When the instance is terminated, you lose any data in the instance store.

**Amazon Elastic Block Store** (Amazon EBS) is a service that provides block-level storage volumes that you can use with Amazon EC2 instances. If you stop or terminate an Amazon EC2 instance, all the data on the attached EBS volume remains available.

Because EBS volumes are for data that needs to persist, it’s important to back up the data. You can take incremental backups of EBS volumes by creating **Amazon EBS snapshots**.
An EBS snapshot is an incremental backup.

## Amazon Simple Storage Service (Amazon S3)

In object storage, each object consists of data, metadata, and a key.

Recall that when you modify a file in block storage, only the pieces that are changed are updated. When a file in object storage is modified, the entire object is updated.

Amazon Simple Storage Service (Amazon S3) is a service that provides object-level storage. Amazon S3 stores data as objects in buckets.

Amazon S3 offers unlimited storage space. The maximum file size for an object in Amazon S3 is 5 TB.

### Amazon S3 storage classes

- S3 Standard:
    - Designed for frequently accessed data
    - Stores data in a minimum of three Availability Zones
- S3 Standard-Infrequent Access (S3 Standard-IA):
    - Ideal for infrequently accessed data
    - Similar to S3 Standard but has a lower storage price and higher retrieval price
- S3 One Zone-Infrequent Access (S3 One Zone-IA):
    - Stores data in a single Availability Zone
    - Has a lower storage price than S3 Standard-IA
    - You want to save costs on storage.
    - You can easily reproduce your data in the event of an Availability Zone failure.
- S3 Intelligent-Tiering:
    - Ideal for data with unknown or changing access patterns
    - Requires a small monthly monitoring and automation fee per object
- S3 Glacier
    - Low-cost storage designed for data archiving
    - Able to retrieve objects within a few minutes to hours
- S3 Glacier Deep Archive
    - Lowest-cost object storage class ideal for archiving
    - Able to retrieve objects within 12 hours

## Amazon Elastic File System (Amazon EFS)

A storage server uses block storage with a local file system to organize files. Clients access data through file paths.

Compared to block storage and object storage, file storage is ideal for use cases in which a large number of services and resources need to access the same data at the same time.

Amazon Elastic File System (Amazon EFS) is a scalable file system used with AWS Cloud services and on-premises resources.
As you add and remove files, Amazon EFS grows and shrinks automatically.
It can scale on demand to petabytes without disrupting applications.

**EBS**:
- An Amazon EBS volume stores data in a single Availability Zone.
- To attach an Amazon EC2 instance to an EBS volume, both the Amazon EC2 instance and the EBS volume must reside within the same Availability Zone.

**EFS**:
- Amazon EFS is a regional service. It stores data in and across multiple Availability Zones.
- The duplicate storage enables you to access data concurrently from all the Availability Zones in the Region where a file system is located. Additionally, on-premises servers can access Amazon EFS using AWS Direct Connect.

## Amazon Relational Database Service (Amazon RDS)

a managed service that enables you to run relational databases in the AWS Cloud...

Amazon RDS provides a number of different security options. Many Amazon RDS database engines offer encryption at rest (protecting data while it is stored) and encryption in transit (protecting data while it is being sent and received).

**Amazon Aurora** is an enterprise-class relational database. It is compatible with MySQL and PostgreSQL relational databases. It is up to five times faster than standard MySQL databases and up to three times faster than standard PostgreSQL databases.

Consider Amazon Aurora if your workloads require high availability. It replicates six copies of your data across three Availability Zones and continuously backs up your data to Amazon S3.

## Amazon DynamoDB

A key-value database service. It delivers single-digit millisecond performance at any scale.

**DynamoDB is serverless**, which means that you do not have to provision, patch, or manage servers.

As the size of your database shrinks or grows, DynamoDB automatically scales to adjust for changes in capacity while maintaining consistent performance. This makes it a suitable choice for use cases that require high performance while scaling.

## Amazon Redshift

Amazon Redshift is a data warehousing service that you can use for big data analytics. It offers the ability to collect data from many sources and helps you to understand relationships and trends across your data.

## AWS Database Migration Service (AWS DMS)

AWS Database Migration Service (AWS DMS) enables you to migrate relational databases, nonrelational databases, and other types of data stores.

use cases:
- Development and test database migrations
- Database consolidation
- Continuous replication

## Additional database services

- Amazon DocumentDB: supports MongoDB workloads.
- Amazon Neptune: graph database service.
- Amazon Quantum Ledger Database (Amazon QLDB): a ledger database service.
- Amazon Managed Blockchain
- Amazon ElastiCache: adds caching layers on top of your databases to help improve the read times of common requests. It supports two types of data stores: Redis and Memcached.
- Amazon DynamoDB Accelerator (DAX): is an in-memory cache for DynamoDB.

# Security

## Shared responsibility model

Customers: Security in the cloud - Customers are responsible for the security of everything that they create and put in the AWS Cloud.

AWS: Security of the cloud (Physical security of data centers / Hardware and software infrastructure / Network infrastructure / Virtualization infrastructure)

Although you cannot visit AWS data centers to see this protection firsthand, AWS provides several reports from third-party auditors. These auditors have verified its compliance with a variety of computer security standards and regulations.

## AWS Identity and Access Management (IAM)

enables you to manage access to AWS services and resources securely.

IAM features:
- IAM users, groups, and roles
- IAM policies
- Multi-factor authentication

When you first create an AWS account, you begin with an identity known as the **root user**.

Best practice: Do not use the root user for everyday tasks.

An **IAM user** is an identity that you create in AWS. It represents the person or application that interacts with AWS services and resources. It consists of a name and credentials.
By default, a new IAM user has no permissions.

An **IAM policy** is a document that allows or denies permissions to AWS services and resources.

Best practice: Follow the security principle of least privilege when granting permissions.

An **IAM group** is a collection of IAM users. When you assign an IAM policy to a group, all users in the group are granted permissions specified by the policy.

An **IAM role** is an identity that you can assume to gain temporary access to permissions.

In IAM, multi-factor authentication (MFA) provides an extra layer of security for your AWS account.


## AWS Organizations

You can use AWS Organizations to consolidate and manage multiple AWS accounts within a central location.

In AWS Organizations, you can centrally control permissions for the accounts in your organization by using **service control policies (SCPs)**. SCPs enable you to place restrictions on the AWS services, resources, and individual API actions that users and roles in each account can access.

In AWS Organizations, you can group accounts into **organizational units (OUs)** to make it easier to manage accounts with similar business or security requirements. When you apply a policy to an OU, all the accounts in the OU automatically inherit the permissions specified in the policy.

## Compliance

**AWS Artifact** is a service that provides on-demand access to AWS security and compliance reports and select online agreements. AWS Artifact consists of two main sections: AWS Artifact Agreements and AWS Artifact Reports.

The **Customer Compliance Center** contains resources to help you learn more about AWS compliance.


## Denial-of-service attacks

**AWS Shield** is a service that protects applications against DDoS attacks:
- AWS Shield Standard: automatically protects all AWS customers at no cost.
- AWS Shield Advanced: a paid service that provides detailed attack diagnostics and the ability to detect and mitigate sophisticated DDoS attacks.

**AWS Key Management Service** (AWS KMS) enables you to perform encryption operations through the use of cryptographic keys.

**AWS WAF** is a web application firewall that lets you monitor network requests that come into your web applications.
Use web access control list (ACL).

**Amazon Inspector** helps to improve the security and compliance of applications by running automated security assessments. It checks applications for security vulnerabilities and deviations from security best practices, such as open access to Amazon EC2 instances and installations of vulnerable software versions.

**Amazon GuardDuty** is a service that provides intelligent threat detection for your AWS infrastructure and resources. It identifies threats by continuously monitoring the network activity and account behavior within your AWS environment.

# Monitoring & analytics

## Amazon CloudWatch

**Amazon CloudWatch** is a web service that enables you to monitor and manage various metrics and configure alarm actions based on data from those metrics.

With CloudWatch, you can create **alarms** that automatically perform actions if the value of your metric has gone above or below a predefined threshold.
You can create a CloudWatch alarm that automatically stops an Amazon EC2 instance when the CPU utilization percentage has remained below a certain threshold for a specified period. When configuring the alarm, you can specify to receive a notification whenever this alarm is triggered.

## AWS CloudTrail

AWS CloudTrail records API calls for your account. The recorded information includes the identity of the API caller, the time of the API call, the source IP address of the API caller, and more. You can think of CloudTrail as a “trail” of breadcrumbs (or a log of actions) that someone has left behind them.

Within CloudTrail, you can also enable **CloudTrail Insights**. This optional feature allows CloudTrail to automatically detect unusual API activities in your AWS account.

## AWS Trusted Advisor

AWS Trusted Advisor is a web service that inspects your AWS environment and provides real-time recommendations in accordance with AWS best practices.
Trusted Advisor compares its findings to AWS best practices in five categories:
- cost optimization
- performance
- security
- fault tolerance
- service limits

# Pricing & Support

## AWS Free Tier

The AWS Free Tier enables you to begin using certain services without having to worry about incurring costs for the specified period.

- Always Free: ex, AWS Lambda allows 1 million free requests and up to 3.2 million seconds of compute time per month. Amazon DynamoDB allows 25 GB of free storage per month.
- 12 Months Free: These offers are free for 12 months **following your initial sign-up date** to AWS. ex, include specific amounts of Amazon S3 Standard Storage, thresholds for monthly hours of Amazon EC2 compute time, and amounts of Amazon CloudFront data transfer out.
- Trials: ex, Amazon Inspector offers a 90-day free trial. Amazon Lightsail (a service that enables you to run virtual private servers) offers 750 free hours of usage over a 30-day period.

## How AWS pricing works

- Pay for what you use.
- Pay less when you reserve.
- Pay less with volume-based discounts when you use more.


### S3 pricing

- Storage - You pay for only the storage that you use. You are charged the rate to store objects in your Amazon S3 buckets based on your objects’ sizes, storage classes, and how long you have stored each object during the month.

- Requests and data retrievals - You pay for requests made to your Amazon S3 objects and buckets. For example, suppose that you are storing photo files in Amazon S3 buckets and hosting them on a website. Every time a visitor requests the website that includes these photo files, this counts towards requests you must pay for.

- Data transfer - There is no cost to transfer data between different Amazon S3 buckets or from Amazon S3 to other services within the same AWS Region. However, you pay for data that you transfer into and out of Amazon S3, with a few exceptions. There is no cost for data transferred into Amazon S3 from the internet or out to Amazon CloudFront. There is also no cost for data transferred out to an Amazon EC2 instance in the same AWS Region as the Amazon S3 bucket.

- Management and replication - You pay for the storage management features that you have enabled on your account’s Amazon S3 buckets. These features include Amazon S3 inventory, analytics, and object tagging.

## Billing dashboard

Use the AWS Billing & Cost Management dashboard to pay your AWS bill, monitor your usage, and analyze and control your costs.

- Compare your current month-to-date balance with the previous month, and get a forecast of the next month based on current usage.
- View month-to-date spend by service.
- View Free Tier usage by service.
- Access Cost Explorer and create budgets.
- Purchase and manage Savings Plans.
- Publish AWS Cost and Usage Reports.

## Consolidated billing

**AWS Organizations** also provides the option for **consolidated billing**.
The consolidated billing feature of AWS Organizations enables you to receive a single bill for all AWS accounts in your organization. By consolidating, you can easily track the combined costs of all the linked accounts in your organization. The default maximum number of accounts allowed for an organization is 4, but you can contact AWS Support to increase your quota, if needed.

Another benefit of consolidated billing is the ability to share bulk discount pricing, Savings Plans, and Reserved Instances across the accounts in your organization. For instance, one account might not have enough monthly usage to qualify for discount pricing.

## AWS Budgets

In AWS Budgets, you can create budgets to plan your service usage, service costs, and instance reservations.

## AWS Cost Explorer

AWS Cost Explorer is a tool that enables you to visualize, understand, and manage your AWS costs and usage over time.

## AWS Support

AWS offers four different Support plans to help you troubleshoot issues, lower costs, and efficiently use AWS services.

- Basic: free for all AWS customers.
- Developer: Best practice guidance, Client-side diagnostic tools, Building-block architecture support, which consists of guidance for how to use AWS offerings, features, and services together
- Business: includes all AWS Trusted Advisor checks at the lowest cost
- Enterprise: Application architecture guidance, Infrastructure event management

The Enterprise Support plan includes access to a **Technical Account Manager (TAM)**.

If your company has an Enterprise Support plan, the TAM is your primary point of contact at AWS. They provide guidance, architectural reviews, and ongoing communication with your company as you plan, deploy, and optimize your applications.

## AWS Marketplace

AWS Marketplace is a digital catalog that includes thousands of software listings from independent software vendors. You can use AWS Marketplace to find, test, and buy software that runs on AWS.

# Migration & Innovation

## AWS Cloud Adoption Framework (AWS CAF)

At the highest level, the AWS Cloud Adoption Framework (AWS CAF) organizes guidance into six areas of focus, called Perspectives. Each Perspective addresses distinct responsibilities. The planning process helps the right people across the organization prepare for the changes ahead.

In general, the Business, People, and Governance Perspectives focus on business capabilities, whereas the Platform, Security, and Operations Perspectives focus on technical capabilities.

- The Business Perspective ensures that IT aligns with business needs and that IT investments link to key business results.
- The People Perspective supports development of an organization-wide change management strategy for successful cloud adoption.
- The Governance Perspective focuses on the skills and processes to align IT strategy with business strategy. This ensures that you maximize the business value and minimize risks.
- The Platform Perspective includes principles and patterns for implementing new solutions on the cloud, and migrating on-premises workloads to the cloud.
- The Security Perspective ensures that the organization meets security objectives for visibility, auditability, control, and agility.
- The Operations Perspective helps you to enable, run, use, operate, and recover IT workloads to the level agreed upon with your business stakeholders.


## Migration strategies

6 strategies for migration

When migrating applications to the cloud, six of the most common migration strategies that you can implement are:
- Rehosting: “lift and shift.”
- Replatforming: involves selectively optimizing aspects of an application to achieve benefits in the cloud without changing the core architecture of the application. It is also known as “lift, tinker, and shift.”
- Refactoring/re-architecting: involves changing how an application is architected and developed, typically by using cloud-native features.
- Repurchasing: involves replacing an existing application with a cloud-based version, such as software found in AWS Marketplace.
- Retaining: consists of keeping applications that are critical for the business in the source environment.
- Retiring: involves removing an application that is no longer used or that can be turned off.

## AWS Snow Family

AWS Snow Family is composed of AWS Snowcone, AWS Snowball, and AWS Snowmobile.

- AWS Snowcone is a small, rugged, and secure edge computing and data transfer device.
It features 2 CPUs, 4 GB of memory, and 8 TB of usable storage.

- Snowball Edge Storage Optimized devices are well suited for large-scale data migrations and recurring transfer workflows, in addition to local computing with higher capacity needs. 80 TB, 40 vCPUs
- Snowball Edge Compute Optimized provides powerful computing resources for use cases such as machine learning, full motion video analysis, analytics, and local computing stacks. 42-TB, 52 vCPUs

- AWS Snowmobile is an exabyte-scale data transfer service used to move large amounts of data to AWS.
You can transfer up to 100 petabytes of data per Snowmobile, a 45-foot long ruggedized shipping container, pulled by a semi trailer truck.

## Innovation with AWS

- Convert speech to text with Amazon Transcribe.
- Discover patterns in text with Amazon Comprehend.
- Identify potentially fraudulent online activities with Amazon Fraud Detector.
- Build voice and text chatbots with Amazon Lex.
- AWS offers Amazon SageMaker to remove the difficult work from the process and empower you to build, train, and deploy ML models quickly.

# Cloud Journey

## The AWS Well-Architected Framework

The AWS Well-Architected Framework helps you understand how to **design and operate reliable, secure, efficient, and cost-effective systems** in the AWS Cloud. It provides a way for you to consistently measure your architecture against best practices and design principles and identify areas for improvement.

The Well-Architected Framework is based on five pillars:
- Operational excellence: ability to run and monitor systems to deliver business value and to continually improve supporting processes and procedures.
- Security: ability to protect information, systems, and assets while delivering business value through risk assessments and mitigation strategies.
- Reliability: ability of a workload to consistently and correctly perform its intended functions
  - Recover from infrastructure or service disruptions
  - Dynamically acquire computing resources to meet demand
  - Mitigate disruptions such as misconfigurations or transient network issues
- Performance efficiency: ability to use computing resources efficiently to meet system requirements and to maintain that efficiency as demand changes and technologies evolve.
- Cost optimization: ability to run systems to deliver business value at the lowest price point.


